
FROM ubuntu:14.04

# Install Nginx.
RUN \
  apt-get update && \
  apt-get install -y software-properties-common && \
  add-apt-repository -y ppa:nginx/stable && \
  apt-get update && \
  apt-get install -y nginx wget curl nano && \
  echo "daemon off;" >> /etc/nginx/nginx.conf

# Install HHVM
RUN \
  wget -O - http://dl.hhvm.com/conf/hhvm.gpg.key | sudo apt-key add - && \
  echo deb http://dl.hhvm.com/ubuntu trusty main | sudo tee /etc/apt/sources.list.d/hhvm.list && \
  apt-get update && \
  apt-get install -y curl hhvm && \
  service hhvm stop && \
  service nginx stop


#install composer
#RUN /usr/bin/curl -sS https://getcomposer.org/installer |/usr/bin/php
#RUN /bin/mv composer.phar /usr/local/bin/composer


# Nginx bridge to HHVM over FastCGI.
RUN \
  /usr/share/hhvm/install_fastcgi.sh && \
  /usr/bin/update-alternatives --install /usr/bin/php php /usr/bin/hhvm 60

# Add vhost
RUN rm /etc/nginx/sites-available/*
RUN rm /etc/nginx/sites-enabled/*
ADD default /etc/nginx/sites-available/default
ADD default /etc/nginx/sites-enabled/default
RUN rm /var/www/html/*

# clone project from git
Run \
  apt-get install -y git && \
  apt-get -yq install ssh && \
  git clone https://sanjid133@bitbucket.org/sanjid133/docker-laravel.git /var/www/html/ && \
  chown -R www-data:www-data /var/www

WORKDIR /var/www/html
#RUN composer install

# Define working directory.
WORKDIR /etc/nginx
# Expose ports.
EXPOSE 80
RUN service hhvm start
RUN service nginx stop
CMD ["nginx"]
ADD index.html /var/www/html/public/index.html
ADD php.php /var/www/html/public/php.php
RUN apt-get install -y runit
RUN ps -ef | grep runsvdir
RUN mkdir /etc/service/nginx
RUN mkdir /etc/service/hhvm
ADD nginx/run /etc/service/nginx/
ADD hhvm/run /etc/service/hhvm/
RUN chmod +x /etc/service/nginx/run
RUN chmod +x /etc/service/hhvm/run
RUN /etc/service/nginx/run
#RUN /etc/service/hhvm/run
#RUN runsvdir -p /etc/service
#RUN sv restart nginx
#RUN sv restart hhvm
RUN hhvm /var/www/html/public/php.php
RUN service nginx status
